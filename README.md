# M300 Backend

Für das Modul 300 wird hier für mein Projekt ein React API Backend zum klonen bereitgestellt.

Das Risiko die Credentials in einem öffentlichen Repository zu haben ist mir klar bewusst, jedoch kann ich den Zeitaufwand zu dieser Zeit nicht mit solchen Sicherheitsmassnahmen rechtfertigen und müssten zu einem späteren Zeitpunkt implementiert werden.