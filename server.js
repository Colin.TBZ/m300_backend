const express = require('express');
const fs = require('fs');
const app = express();
const https = require('https');
const nodemailer = require('nodemailer');
const promClient = require('prom-client');
const cors = require('cors');
const path = require('path');
const { Pool } = require('pg');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser'); 
const { Readable } = require('stream');

const fileUploadsTotal = new promClient.Counter({
  name: 'file_uploads_total',
  help: 'Total number of file uploads'
});

const httpRequestsTotal = new promClient.Counter({
  name: 'http_requests_total',
  help: 'Total number of HTTP requests',
  labelNames: ['method', 'path', 'status_code'] 
});

const options = {
  key: fs.readFileSync('/etc/ssl/nginx-selfsigned.key'),
  cert: fs.readFileSync('/etc/ssl/nginx-selfsigned.crt')
};

const httpsServer = https.createServer(options, app);

app.use(cors({ origin: 'https://172.31.177.161' }));
app.use(fileUpload());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  const end = res.end;
  res.end = (chunk, encoding) => {
      httpRequestsTotal.inc({ 
          method: req.method, 
          path: req.path, 
          status_code: res.statusCode 
      });
      res.end = end;
      res.end(chunk, encoding);
  };
  next();
});

app.post('/api/upload', async (req, res) => {
  console.log('req.files:', req.files);
  console.log('req.body:', req.body);
  console.log('req.headers:', req.headers);
  if (!req.files || !req.files.file) {
      return res.status(400).json({ success: false, message: 'No file uploaded' }); 
  }

  const file = req.files.file; // Access the uploaded file

  const metadata = {
      filename: file.name, 
      originalName: file.name, 
      mimetype: file.mimetype,
      size: file.size,
      category: req.body.category || 'uncategorized',
  };

  try {
      const client = await pool.connect();
      try {
          await client.query('BEGIN');

          const insertQuery = `INSERT INTO files.uploads (filename, original_name, mimetype, size, category, data) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id`;
          const insertResult = await client.query(insertQuery, [
              metadata.filename,
              metadata.originalName,
              metadata.mimetype,
              metadata.size,
              metadata.category,
              file.data 
          ]);

          const uploadedId = insertResult.rows[0].id;
          await client.query('COMMIT');

          fileUploadsTotal.inc();

          const transporter = nodemailer.createTransport({
              host: '172.31.177.161', 
              port: 1025,  
              secure: false,  
              tls: { 
                  rejectUnauthorized: false 
              } 
          });

          const mailOptions = {
              from: 'notification@user33e.xyz', 
              to: 'enduser@test.com', 
              subject: 'File Upload Notification',
              text: `A new file has been uploaded:\n` +
                    `File Name: ${metadata.filename}\n` +
                    `Category: ${metadata.category}\n` 
          };

          await transporter.sendMail(mailOptions); 
          console.log('Notification email sent!'); 

          res.json({
              success: true,
              file: metadata,
          });

      } catch (error) {
          await client.query('ROLLBACK');
          console.error('Error uploading file and metadata:', error);
          res.json({ success: false, message: 'Upload failed' });
      } finally {
          client.release();
      }

  } catch (error) {
      console.error('Error connecting to database:', error);
      res.json({ success: false, message: 'Upload failed' });
  }
});


app.get('/api/files', async (req, res) => {
  const client = await pool.connect();
  try {
    const result = await client.query('SELECT id, filename, original_name, mimetype, size, category FROM files.uploads');
    const files = result.rows;
    res.json(files);
  } catch (error) {
    console.error('Error fetching files:', error);
    res.json({ success: false, message: 'Failed to fetch files' });
  } finally {
    client.release(); // Verbindung freigeben
  }
});

app.get('/api/download/:file', async (req, res) => {
  const client = await pool.connect();
  try {
    const queryResult = await client.query(
      'SELECT id, filename, original_name, mimetype, data FROM files.uploads WHERE filename = $1',
      [req.params.file]
    );

    if (queryResult.rowCount === 0) {
      return res.status(404).json({ success: false, message: 'File not found' });
    }

    const fileInfo = queryResult.rows[0];

    res.set({
      'Content-Type': fileInfo.mimetype,
      'Content-Disposition': `attachment; filename="${fileInfo.original_name}"`,
    });

    const fileData = queryResult.rows[0].data; // Access the data directly from the query result

    const readableStream = new Readable();
    readableStream._read = () => {}; // Required for Readable stream
    readableStream.push(fileData);
    readableStream.push(null); // Signals the end of the stream

    readableStream.pipe(res);

  } catch (error) {
    console.error('Error downloading file:', error);
    res.status(500).json({ success: false, message: 'Error downloading file' });
  } finally {
    client.release();
  }
});

app.get('/metrics', async (req, res) => {
  try {
      res.set('Content-Type', promClient.register.contentType);
      res.end(await promClient.register.metrics()); 
  } catch (error) {
      res.status(500).send('Error generating metrics');
  }
});


const pool = new Pool({
  user: 'postgres',
  host: '172.31.177.161', 
  database: 'thirdupload',
  password: 'postgres',
  port: 5432,
});

async function connectToDatabase() {
  try {
    await pool.connect();
    console.log('Connected to database');
  } catch (error) {
    console.error('Database connection error:', error);
    process.exit(1);
  }
}

connectToDatabase();

httpsServer.listen(8604, () => {
  console.log('Server läuft auf Port 8604 mit HTTPS :)');
});

/*
app.listen(8604, () => {
  console.log('Server läuft auf Port 8604');
});
*/
